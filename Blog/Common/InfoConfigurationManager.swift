//
//  InfoConfigurationManager.swift
//  Blog
//
//  Created by Pablo Bórquez on 08/04/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

class InfoConfigurationManager {
    
    private var sections: NSDictionary!
    
    private let fileName = "Info"
    private let documentExtension = "plist"
    
    private let kVersion = "CFBundleShortVersionString"
    
    init() {
        let pListUrl = NSBundle.mainBundle().URLForResource(fileName, withExtension: documentExtension)
        sections = NSDictionary(contentsOfURL: pListUrl!) as NSDictionary!
    }
    
    func bundleVersion() -> String {
        return self.sections[kVersion] as! String
    }
}