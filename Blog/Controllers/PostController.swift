//
//  AddPostController.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

/// PostController class handles the interaction between the view and the model
class PostController {
    
    // MARK: - let - structs
    
    private let restHelper = RESTHelper()
    
    struct PostDictionaryStruct {
        static let title = "title"
        static let author = "author"
        static let date = "date"
        static let content = "content"
    }
    
    // MARK: - public funcs
    
    /**
        Saves the Post on web server and stores it on internal database.
        - Parameter postToCreate:       Post to save.
        - Parameter completionHandler:  The block to execute after try to save the Post on web server
                                        and stored on internal database. Success returns true if the process
                                        execution was successful and message is a message when the process
                                        execution fails.
    */
    func createPost(postToCreate: PostStruct, completionHandler: (success: Bool, message: String) -> Void) {
        
        let postDictionary = [
            PostDictionaryStruct.title : postToCreate.title,
            PostDictionaryStruct.author : postToCreate.author,
            PostDictionaryStruct.content : postToCreate.content,
            PostDictionaryStruct.date : postToCreate.creationDate.fullStringTime()
        ];
        
        restHelper.post(postDictionary, urlString: ConfigurationStruct.postUrl) { (success, message) -> Void in
            self.storeOnInternalDatabasePostStruct(postToCreate)
            completionHandler(success: success, message: message)
        }
    }
    
    /**
        Gets Post from web server or from internal database. If is not posible to get the Posts from the web
        server, it gets from internal database.
        - Parameter completionHandler:  The block to execute after try to get the Posts. Success returns true
                                        if the process execution was successful. Posts return the Posts stored
                                        on web server or internal database. Message is a message when the process
                                        execution fails.
    */
    func getPosts(completionHandler: (success: Bool, posts: [PostStruct], message: String) -> Void) {
        
        restHelper.get(ConfigurationStruct.getUrl) { (success, responseData, message) -> Void in
            var postsStruct = [PostStruct]()
            if !success {
                postsStruct = self.postStructStoredOnInternalDatabase()
                completionHandler(success: false, posts: postsStruct, message: message)
                
                return
            }
            
            do {
                let postsSavedOnServerArray : NSArray = try NSJSONSerialization.JSONObjectWithData(responseData!, options: NSJSONReadingOptions.AllowFragments) as! NSArray
                
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                for postSavedOnServer in postsSavedOnServerArray {
                    let postCreationDateString = postSavedOnServer.objectForKey(PostDictionaryStruct.date) as! String
                    
                    let post = PostStruct(
                        title: postSavedOnServer.objectForKey(PostDictionaryStruct.title) as! String,
                        author: postSavedOnServer.objectForKey(PostDictionaryStruct.author) as! String,
                        creationDate:  dateFormatter.dateFromString(postCreationDateString)!,
                        content: postSavedOnServer.objectForKey(PostDictionaryStruct.content) as! String)
                    
                    postsStruct.append(post)
                }
                
                self.storeOnInternalDatabasePostStructArray(postsStruct)
                completionHandler(success: true, posts: postsStruct, message: "")
            } catch {
                postsStruct = self.postStructStoredOnInternalDatabase()
                completionHandler(success: false, posts: postsStruct, message: NSLocalizedString(StoryboardStringStruct.RESTHelperErrorKey, comment: ""))
            }
        }
    }
    
    // MARK: - private funcs
    
    private func postStructStoredOnInternalDatabase() -> [PostStruct] {
        var postsStruct = [PostStruct]()
        let realm = try! Realm()
        
        for postRealm in realm.objects(Post) {
            let post = PostStruct(
                title: postRealm.title!,
                author: postRealm.author!,
                creationDate: postRealm.creationDate!,
                content: postRealm.content!)
            
            postsStruct.append(post)
        }
        
        return postsStruct
    }
    
    private func storeOnInternalDatabasePostStruct(postStructToStore: PostStruct) {
        let post = Post()
        
        post.title = postStructToStore.title
        post.author = postStructToStore.author
        post.content = postStructToStore.content
        post.creationDate = postStructToStore.creationDate
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(post)
        }
    }
    
    private func storeOnInternalDatabasePostStructArray(postStructArraytoStore: [PostStruct]) {
        
        let realm = try! Realm()
        
        try! realm.write {
            realm.delete(realm.objects(Post))
        }
        
        for postStructToStore in postStructArraytoStore {
            storeOnInternalDatabasePostStruct(postStructToStore)
        }
    }
}

