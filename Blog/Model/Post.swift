//
//  Post.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import RealmSwift

class Post: Object {
    dynamic var title: String?
    dynamic var author: String?
    dynamic var content: String?
    dynamic var creationDate: NSDate?
}
