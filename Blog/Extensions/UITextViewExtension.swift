//
//  UITextViewExtension.swift
//  Blog
//
//  Created by pablo borquez on 19/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

extension UITextView {
    
    // MARK: - public funcs
    
    /**
        Adds a placeholder
        - Parameter placeholder:                Placeholder text
    */
    func addPlaceholder(placeholder: String) {
        let placeholderContentTextLabel = UILabel()
        
        placeholderContentTextLabel.text = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerContentPlaceholderKey, comment: "")
        placeholderContentTextLabel.font = UIFont.italicSystemFontOfSize(self.font!.pointSize)
        placeholderContentTextLabel.sizeToFit()
        
        self.addSubview(placeholderContentTextLabel)
        
        updatePlaceholderContentTextLabelFrame(placeholderContentTextLabel)
        placeholderContentTextLabel.frame.origin = CGPointMake(5, self.font!.pointSize / 2)

        placeholderContentTextLabel.textColor = UIColor(white: 0, alpha: 0.3)
        placeholderContentTextLabel.hidden = !self.text.isEmpty
    }
    
    /**
        This method it must implement on textFieldShouldReturn: from UITextView delegete
        only work if the text view doesn't have a custom UIView.
        - Parameter hide:                       true if you want to hide the palceholder.
    */
    func hidePlaceholder(hide: Bool) {
        if let placeholderContentTextLabel = obtainPlaceholderContentTextLabel() {
            placeholderContentTextLabel.hidden = hide
        }
    }
    
    /**
        Changes placeholder font size for preferred font size.
        - Parameter preferredCGFloatFontSize:   Preferred font size.
    */
    func changeForPreferredPlaceholderFontSize(preferredCGFloatFontSize: CGFloat) {
        if let placeholderContentTextLabel = obtainPlaceholderContentTextLabel() {
            updatePlaceholderContentTextLabelFrame(placeholderContentTextLabel)
            placeholderContentTextLabel.font = UIFont.italicSystemFontOfSize(preferredCGFloatFontSize)
        }
    }
    
    // MARK: - private funcs
    
    private func obtainPlaceholderContentTextLabel() -> UILabel?{
        for view in self.subviews {
            if let placeholderContentTextLabel = view as? UILabel {
                return placeholderContentTextLabel
            }
        }
        
        return nil
    }
    
    private func updatePlaceholderContentTextLabelFrame(placeholderContentTextLabelToUpdate: UILabel) {
        placeholderContentTextLabelToUpdate.frame.size.width = self.frame.width
    }
}