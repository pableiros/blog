//
//  StringExtension.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

extension String {
    
    /**
        Check if the string is empty with white spaces.
        - returns: true if is empty with white spaces, false if is not empty with white spaces
    */
    func isFullEmpty() -> Bool {
        let trimmedString = self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return trimmedString.isEmpty
    }
    
    func translate() -> String {
        return NSLocalizedString(self, comment: "")
    }
}