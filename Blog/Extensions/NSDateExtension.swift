//
//  NSDateExtension.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

extension NSDate {
    
    /// Date depending on the area in which the device is set
    func localizedStringTime()->String {
        return NSDateFormatter.localizedStringFromDate(self, dateStyle: NSDateFormatterStyle.MediumStyle, timeStyle: NSDateFormatterStyle.NoStyle)
    }
    
    /// Full string date time
    func fullStringTime() -> String {
        let dateFormatter = NSDateFormatter()
        
        let theDateFormat = NSDateFormatterStyle.MediumStyle
        let theTimeFormat = NSDateFormatterStyle.MediumStyle
        
        dateFormatter.dateStyle = theDateFormat
        dateFormatter.timeStyle = theTimeFormat
        
        return dateFormatter.stringFromDate(self)
    }
}