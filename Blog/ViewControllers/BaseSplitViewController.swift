//
//  BaseSplitViewController.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import UIKit

class BaseSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
    
    // MARK: - override super funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredDisplayMode = UISplitViewControllerDisplayMode.AllVisible
        self.delegate = self
    }
    
    // MARK: - Split view controller delegate
    
    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool{
        return true
    }
}