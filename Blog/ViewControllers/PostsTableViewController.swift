//
//  PostsTableViewController.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import UIKit

class PostsTableViewController: BaseTableViewController, UISearchBarDelegate {

    // MARK: - @IBOulets
    
    @IBOutlet weak var addPostBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var postSearchBar: UISearchBar!
    
    // MARK: - vars - lets
    
    private var postsStructs: [PostStruct]? {
        didSet {
            postSearchBar.userInteractionEnabled = postsStructs?.count > 0 ? true : false
            postShown = [Bool](count: (postsStructs?.count)!, repeatedValue: false)
        }
    }
    
    private var loadingPostsSpinningWheel : UIActivityIndicatorView?
    private var noPostsTableViewLabel: UILabel?
    private var alertController: UIAlertController?
    private var titleNavigationBarItem: UIView?
    private var tableBackgroundView: UIView?
    private var selectedIndexPath: NSIndexPath?
    private var allPostsStructs: [PostStruct]?
    
    private var navigationTitle = NSLocalizedString(StoryboardStringStruct.PostTableViewControllerTitleKey, comment: "")
    private var postShown = [Bool]()
    private var canDisplayUIComponents: Bool = true

    private let estimateRowHeight = CGFloat(220)
    
    // MARK: - override super funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeComponents()

        networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler.initialiceWithViewController(self, andNavigationTitle: StoryboardStringStruct.PostTableViewControllerTitleKey.translate())
        
        loadPostsStructs({})
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationHelper.addObserverNotificationOnMainQueue(self, selector: #selector(PostsTableViewController.contentSizeCategoryDidChangeNotification(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
  
        canDisplayUIComponents = true
        
        self.refreshControl?.endRefreshing()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationHelper.removeObserverNotification(self)
        
        canDisplayUIComponents = false
    }
    
    // MARK: - public funcs
    
    func contentSizeCategoryDidChangeNotification(notification: NSNotification) {
        tableView.reloadData()
    }
    
    func pullToReloadData() {
        loadPostsStructs() {
            let lastUpdateString = "\(NSLocalizedString(StoryboardStringStruct.PostTableViewControllerLastUpdateKey, comment: ""))  \(NSDate().fullStringTime())"
            let lastUpdateAttrDictionary = [NSForegroundColorAttributeName : UIColor.grayColor()]
            let lastUpdateAttributedString = NSAttributedString(string: lastUpdateString, attributes: lastUpdateAttrDictionary)
            
            self.refreshControl?.attributedTitle = lastUpdateAttributedString
            self.refreshControl?.endRefreshing()
        }
    }
    
    // MARK: - private funcs
    
    private func initializeComponents() {
        titleNavigationBarItem = navigationItem.titleView
        tableBackgroundView = tableView.backgroundView
        
        if NetworkHelper.isConnectedToNetwork() {
            configureViewForInternetConnection()
        } else {
           configureViewForNoInternetConnection()
        }
        
        refreshControl = UIRefreshControl()
        refreshControl?.backgroundColor = UIColor.whiteColor()
        refreshControl?.addTarget(self, action: #selector(PostsTableViewController.pullToReloadData), forControlEvents: UIControlEvents.ValueChanged)
        
        tableView.estimatedRowHeight = estimateRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)
        
        postSearchBar.enablesReturnKeyAutomatically = false
        postSearchBar.userInteractionEnabled = false
        postSearchBar.placeholder = NSLocalizedString(StoryboardStringStruct.PostTableViewControllerSearchPlaceholderKey, comment: "")
    }
    
    private func loadPostsStructs(completionHandler:() -> Void) {
        let postController = PostController()
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        postController.getPosts { (success, posts, message) -> Void in
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            completionHandler()
            
            self.postsStructs = posts
            self.allPostsStructs = posts
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableView.reloadData()
            })
            
            if success == false && self.canDisplayUIComponents == true {
                self.alertController = AlertControllerHelper.alertControllerWithTitle(NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperTitleKey, comment: ""), message: message)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.presentViewController(self.alertController!, animated: true, completion: nil)
                })
            }
        }
    }
    
    // MARK: - Internet connection configurations
    
    override func configureViewForInternetConnection() {
        navigationItem.titleView = titleNavigationBarItem
        navigationItem.title = navigationTitle
        addPostBarButtonItem.enabled = true
        tableView.bounces = true
    }
    
    override func configureViewForNoInternetConnection() {
        
        addPostBarButtonItem.enabled = false
        tableView.bounces = false
    }
    
    // MARK: - @IBActions
    
    @IBAction func addPostUnwindSegue(sender: UIStoryboardSegue) {
        loadPostsStructs({})
    }
    
    // MARK: - Search Bar delegate
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if searchBar == postSearchBar {
            if postSearchBar.text?.isEmpty == true {
                postsStructs = allPostsStructs
            } else {
                var postSearchResults = [PostStruct]()
                let searchQuery = postSearchBar.text!.lowercaseString
                
                for post in allPostsStructs! {
                    var postFound = false
                    
                    if post.title.lowercaseString.rangeOfString(searchQuery) != nil{
                        postFound = true
                    } else if  post.author.lowercaseString.rangeOfString(searchQuery) != nil {
                         postFound = true
                    } else if post.content.lowercaseString.rangeOfString(searchQuery) != nil {
                         postFound = true
                    }

                    if postFound {
                        postSearchResults.append(post)
                    }
                }
                
                postsStructs = postSearchResults
            }
            
            searchBar.resignFirstResponder()
            tableView.reloadData()
        }
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if  searchBar.text?.characters.count == 0 {
            postsStructs = allPostsStructs
            tableView.reloadData()
        }
    }
    
    // MARK: - Table view delegate - data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if postsStructs == nil {
            
            loadingPostsSpinningWheel = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
            loadingPostsSpinningWheel?.frame = CGRect(x: 0, y: 0, width: 150, height: 150)
            loadingPostsSpinningWheel?.startAnimating()
            
            tableView.backgroundView = loadingPostsSpinningWheel
            tableView.separatorStyle = .None
            
            return 0
        } else {
            if postsStructs!.count > 0 {
                noPostsTableViewLabel?.text = ""
                loadingPostsSpinningWheel?.stopAnimating()
                
                return 1
            } else {
                noPostsTableViewLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
                noPostsTableViewLabel!.text = NSLocalizedString(StoryboardStringStruct.PostTableViewControllerNoDataKey, comment: "");
                noPostsTableViewLabel!.textColor = UIColor.grayColor();
                noPostsTableViewLabel!.numberOfLines = 0;
                noPostsTableViewLabel?.textAlignment = NSTextAlignment.Center
                noPostsTableViewLabel!.font = UIFont(name: "Futura", size: 20)
                noPostsTableViewLabel!.sizeToFit();
                tableView.backgroundView = noPostsTableViewLabel
                tableView.separatorStyle = .None

                return 0
            }
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsStructs!.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Post Cell", forIndexPath: indexPath) as! PostTableViewCell
        
        let post = postsStructs![indexPath.row]

        var postContent = post.content
        if post.content.characters.count > 70 {
            let contentRange = post.content.startIndex..<post.content.startIndex.advancedBy(70)
            postContent = post.content.substringWithRange(contentRange) + "..."
        }
        
        cell.titleLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleHeadline)
        cell.authorLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleSubheadline)
        cell.contentLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleBody)
        cell.creationDateLabel.font = UIFont.preferredFontForTextStyle(UIFontTextStyleFootnote)
        
        cell.titleLabel.text = post.title
        cell.authorLabel.text = NSLocalizedString(StoryboardStringStruct.PostTableViewControllerAuthorKey, comment: "") + " " + post.author
        cell.contentLabel.text = postContent
        cell.contentLabel.sizeToFit()
        cell.creationDateLabel.text = NSLocalizedString(StoryboardStringStruct.PostTableViewControllerDateKey, comment: "") + " " + post.creationDate.localizedStringTime()
        
        cell.backgroundColor = cell.contentView.backgroundColor;

        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedIndexPath = indexPath
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.performSegueWithIdentifier("Show Full Post Segue", sender: self)
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if postShown[indexPath.row] == false {
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity, -500, 10, 0)
            
            cell.layer.transform = rotationTransform
            
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                cell.layer.transform = CATransform3DIdentity
            })
            
            postShown[indexPath.row] = true
        }
    }
    
    override func scrollViewDidScroll(scrollView: UIScrollView) {
        postSearchBar.resignFirstResponder()
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let fullPostTableViewController = (segue.destinationViewController as? UINavigationController)?.topViewController as? FullPostTableViewController {
            fullPostTableViewController.post = postsStructs![(selectedIndexPath?.row)!]
        }
    }
}
