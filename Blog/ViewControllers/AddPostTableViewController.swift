//
//  AddPostTableViewController.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import UIKit
import Reachability
import RealmSwift

class AddPostTableViewController: BaseTableViewController, UITextViewDelegate, UITextFieldDelegate {

    // MARK: - @IBOulets
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var authorTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var creationDateLabel: UILabel!
    @IBOutlet weak var creationDateValueLabel: UILabel!
    @IBOutlet weak var noInternetConnectionView: UIView!
    @IBOutlet weak var noInternetConnectionLabel: UILabel!
    @IBOutlet var versionLabel: UILabel!
    
    // MARK: - vars - lets

    private var alertController: UIAlertController?
    private var titleNavigationBarItem: UIView?
    private var loadingViewController: LoadingViewController?
    
    private var creationPostStringDate = NSDate().localizedStringTime()
    private var creationDate = NSDate()
    private var navigationTitle = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerTitleKey, comment: "")
    
    private let creationDateSectionHeaderTitleIndex = 1
    private let systemFontSize = 14.0
    
    // MARK: - override super funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler.initialiceWithViewController(self, andNavigationTitle: StoryboardStringStruct.AddPostTableViewControllerTitleKey.translate())
        
        initializeComponents()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationHelper.addObserverNotificationOnMainQueue(self, selector: #selector(AddPostTableViewController.contentSizeCategoryDidChangeNotification(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
    
        NotificationHelper.removeObserverNotification(self)
        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - public funcs
    
    func contentSizeCategoryDidChangeNotification(notification: NSNotification) {
        let contentSizeCategoryNewValueKey = notification.userInfo![UIContentSizeCategoryNewValueKey]
        let fontHelper = UIFontHelper()
        
        let preferredSystemFontSize = Float(fontHelper.preferredFontSize(contentSizeCategoryNewValueKey as! String))
        
        changeForPreferredFontSize(preferredSystemFontSize)
    }
    
    func hideKeyboardTappingTableView() {
        titleTextField.resignFirstResponder()
        authorTextField.resignFirstResponder()
        contentTextView.resignFirstResponder()
    }
    
    // MARK: - private funcs

    private func prepareVersionUI() {
        
        let infoConfigurationManger = InfoConfigurationManager()
        versionLabel.text = "\(StoryboardStringStruct.AddPostTableViewControllerVersionKey.translate()) \(infoConfigurationManger.bundleVersion())"
    }
    
    private func initializeComponents() {
        title = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerTitleKey, comment: "")
        
        saveBarButtonItem.title = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerSaveKey, comment: "")
        cancelBarButtonItem.title = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerCancelKey, comment: "")
        titleTextField.placeholder = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerPostTitleKey, comment: "")
        authorTextField.placeholder = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerAuthorKey, comment: "")
        noInternetConnectionLabel.text = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerInternetRequiredKey, comment: "")
        creationDateLabel.text = NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerDateKey, comment: "")
        creationDateValueLabel.text = creationPostStringDate
        
        contentTextView.delegate = self
        contentTextView.addPlaceholder(NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerContentPlaceholderKey, comment: ""))
        
        noInternetConnectionView.hidden = true
        alertController = nil
        loadingViewController = nil
        
        tableView.bounces = false
        
        let tapTableViewGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddPostTableViewController.hideKeyboardTappingTableView))
        tableView.addGestureRecognizer(tapTableViewGestureRecognizer)
        
        let fontHelper = UIFontHelper()
        
        let preferredFontSizeAgregator = Float(fontHelper.preferredFontSize(UIApplication.sharedApplication().preferredContentSizeCategory))
       
        changeForPreferredFontSize(preferredFontSizeAgregator)
    
        prepareVersionUI()
    }
    
    private func changeForPreferredFontSize(preferredSystemFontSize: Float) {
        let preferredFontSize = Float(systemFontSize) + preferredSystemFontSize
        let preferredCGFloatFontSize = CGFloat(preferredFontSize)
        let systemFont = UIFont.systemFontOfSize(preferredCGFloatFontSize)
        
        titleTextField.font = systemFont
        authorTextField.font = systemFont
        contentTextView.font = systemFont
        creationDateLabel.font = systemFont
        creationDateValueLabel.font = systemFont
        
        contentTextView.changeForPreferredPlaceholderFontSize(preferredCGFloatFontSize)
    }
    
    private func messageFromEmptyValues() -> String? {
        
        if (titleTextField.text?.isFullEmpty() == true) {
            return NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerPostTitleEmptyKey, comment: "")
        }
        
        if (authorTextField.text?.isFullEmpty() == true) {
            return NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerAuthorEmptyKey, comment: "")
        }
        
        if contentTextView.text.isFullEmpty() {
            return NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerContentEmptyKey, comment: "")
        }
        
        return nil
    }
    
    // MARK: - Internet connection configurations
    
    override func configureViewForInternetConnection() {
        self.noInternetConnectionView.hidden = true
        self.saveBarButtonItem.enabled = true;
        navigationItem.titleView = titleNavigationBarItem
        navigationItem.title = navigationTitle
    }
    
    override func configureViewForNoInternetConnection() {
        self.noInternetConnectionView.hidden = false
        self.saveBarButtonItem.enabled = false;
    }
    
    // MARK: - @IBActions
    
    @IBAction func cancelBarButtonTapped(sender: UIBarButtonItem) {
        alertController = AlertControllerHelper.alertControllerWithTitle(
            NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperTitleKey, comment: ""),
            message: NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerCancelMessageKey, comment: ""))
            { (action) -> Void in
                
                self.dismissViewControllerAnimated(true, completion: nil)
            }
        
        self.presentViewController(alertController!, animated: true, completion: nil)
    }
    
    @IBAction func saveBarButtonTapped(sender: UIBarButtonItem) {
        
        titleTextField.resignFirstResponder()
        authorTextField.resignFirstResponder()
        contentTextView.resignFirstResponder()
        
        if let emptyMessage = messageFromEmptyValues() {
            let alertController = AlertControllerHelper.alertControllerWithTitle(NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperTitleKey, comment: ""), message: emptyMessage)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        } else {
            let postController = PostController()
            let postToCreate = PostStruct(
                title: titleTextField.text!,
                author: authorTextField.text!,
                creationDate: creationDate,
                content: contentTextView.text)
            
            loadingViewController = LoadingViewController(message: NSLocalizedString(StoryboardStringStruct.AddPostTableViewControllerSavingKey, comment: ""))
            presentViewController(loadingViewController!, animated: true, completion: nil)
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            
            postController.createPost(postToCreate, completionHandler: { (success, message) -> Void in
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
               
                if success {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.loadingViewController!.dismissViewControllerAnimated(true, completion: {
                            self.performSegueWithIdentifier("Add Post Unwind Segue", sender: self)
                        })
                    })
                } else {
                    self.alertController = AlertControllerHelper.alertControllerWithTitle(NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperTitleKey, comment: ""), message: message)
                    dispatch_async (dispatch_get_main_queue(), {
                        self.loadingViewController!.dismissViewControllerAnimated(true, completion: {
                            self.presentViewController(self.alertController!, animated: true, completion: nil)
                        })
                    });
                }
            });
        }
    }
    
    // MARK: - Textfield Delegate
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Textview Delegate
    
    func textViewDidChange(textView: UITextView) {
        textView.hidePlaceholder(!textView.text.isEmpty)
    }
}


