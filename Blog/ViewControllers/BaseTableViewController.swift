//
//  BaseTableViewController.swift
//  Blog
//
//  Created by Developer on 05/04/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import UIKit

class BaseTableViewController: UITableViewController {
    
    var networkConnectionViewControllerHandler: NetworkConnectionViewControllerHandler!
    var noInternetObserver: NSObjectProtocol!
    var internetObserver: NSObjectProtocol!
    var internetObserversHandlerTuple: (NSObjectProtocol, NSObjectProtocol) {
        set {
            observersHandlerTuple = newValue
            
            noInternetObserver = observersHandlerTuple.0
            internetObserver = observersHandlerTuple.1
        }
        
        get {
            return observersHandlerTuple
        }
    }
    
    private var observersHandlerTuple: (NSObjectProtocol, NSObjectProtocol)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        internetObserversHandlerTuple = networkConnectionViewControllerHandler.startHandlingNetworkConnectionChanges({
            self.configureViewForNoInternetConnection()
            
        }) {
            self.configureViewForInternetConnection()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        NotificationHelper.removeObserverNotification(noInternetObserver)
        NotificationHelper.removeObserverNotification(internetObserver)
        
        super.viewWillDisappear(animated)
    }
    
    func configureViewForNoInternetConnection() { }
    
    func configureViewForInternetConnection() { } 
}
