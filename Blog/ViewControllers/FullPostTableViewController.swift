//
//  FullPostTableViewController.swift
//  Blog
//
//  Created by pablo borquez on 17/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import UIKit

class FullPostTableViewController: BaseTableViewController {

    // MARK: - vars - lets
    
    var post: PostStruct?
    
    private var preferredFontSizeAgregator: Float = 0
    
    private let estimateRowHeight = CGFloat(265)
    private let postTitleFontName = "TimesNewRomanPS-BoldMT"
    private let publishedFontName = "HelveticaNeue-LightItalic"
    private let contentFontName = "Verdana"
    private let postTitleFontSize = CGFloat(27)
    private let publishedFontSize = CGFloat(14)
    private let contentFontSize = CGFloat(15)

    // MARK: - override super funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler.initialiceWithViewController(self, andNavigationTitle: StoryboardStringStruct.FullPostTableViewControllerTitleKey.translate())
        
        initializeComponents()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NotificationHelper.addObserverNotificationOnMainQueue(self, selector: #selector(FullPostTableViewController.contentSizeCategoryDidChangeNotification(_:)), name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        
        NotificationHelper.removeObserverNotification(self)
        
        super.viewWillDisappear(animated)
    }
    
    // MARK: - public funcs
    
    func contentSizeCategoryDidChangeNotification(notification: NSNotification) {
        let contentSizeCategoryNewValueKey = notification.userInfo![UIContentSizeCategoryNewValueKey]
        let fontHelper = UIFontHelper()
        
        preferredFontSizeAgregator = Float(fontHelper.preferredFontSize(contentSizeCategoryNewValueKey as! String))
        
        tableView.reloadData()
    }
    
    // MARK: - private funcs
    
    private func initializeComponents() {
        tableView.bounces = false
        tableView.estimatedRowHeight = estimateRowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
        
        navigationItem.title = NSLocalizedString(StoryboardStringStruct.FullPostTableViewControllerTitleKey, comment: "")
        
        let fontHelper = UIFontHelper()
        
        preferredFontSizeAgregator = Float(fontHelper.preferredFontSize(UIApplication.sharedApplication().preferredContentSizeCategory))
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if post == nil {
            return 0
        }
        
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Full Post Cell", forIndexPath: indexPath) as! PostTableViewCell

        let postedByString = NSLocalizedString(StoryboardStringStruct.FullPostTableViewControllerPostedKey, comment: "")
        let onString = NSLocalizedString(StoryboardStringStruct.FullPostTableViewControllerOnKey, comment: "")
        
        let titleLabelFontSize = Float(postTitleFontSize) + preferredFontSizeAgregator
        let authorLabelFontSize = Float(publishedFontSize) + preferredFontSizeAgregator
        let contentLabelFontSize = Float(contentFontSize) + preferredFontSizeAgregator
        
        cell.titleLabel.font = UIFont(name: postTitleFontName, size: CGFloat(titleLabelFontSize))
        cell.authorLabel.font = UIFont(name: publishedFontName, size: CGFloat(authorLabelFontSize))
        cell.contentLabel.font = UIFont(name: contentFontName, size: CGFloat(contentLabelFontSize))
        
        cell.titleLabel.text = post?.title
        cell.authorLabel.text = "\(postedByString) \(post!.author) \(onString) \(post!.creationDate.localizedStringTime())"
        cell.contentLabel.text = post?.content
        cell.contentLabel.sizeToFit()
        
        cell.backgroundColor = cell.contentView.backgroundColor;
        
        return cell
    }    
}
