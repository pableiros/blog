//
//  NetworkConnectionViewControllerHandler.swift
//  MFinanzas Lite
//
//  Created by Pablo Bórquez on 31/03/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit

class NetworkConnectionViewControllerHandler {
    
    var isInternetConnection = true
    
    private var viewController: UIViewController!
    private var titleNavigationBarItem: UIView?
    
    private var navigationTitle: String!
    
    private var noInternetConnectionCompletionHandler: (() -> Void)!
    private var internetConnectionCompletionHandler: (() -> Void)!
    
    private init(){}
    
    static func initialiceWithViewController(viewController: UIViewController, andNavigationTitle navigationTitle: String) -> NetworkConnectionViewControllerHandler {
        let networkConnectionViewControllerHandler = NetworkConnectionViewControllerHandler()
        
        networkConnectionViewControllerHandler.viewController = viewController
        networkConnectionViewControllerHandler.navigationTitle = navigationTitle
        
        return networkConnectionViewControllerHandler
    }
    
    func startHandlingNetworkConnectionChanges(noInternetConnectionCompletionHandler:() -> Void, internetConnectionCompletionHandler:() -> Void) -> (noInternetObserver: NSObjectProtocol, internetObserver: NSObjectProtocol) {
        
        self.noInternetConnectionCompletionHandler = noInternetConnectionCompletionHandler
        self.internetConnectionCompletionHandler = internetConnectionCompletionHandler
        
        verifyInternetConnections()
        return addInternetChangesNotifications()
    }
    
    private func verifyInternetConnections() {
        if NetworkHelper.isConnectedToNetwork() {
            configureViewForInternetConnection()
        } else {
            configureViewForNoInternetConnection()
        }
    }
    
    private func addInternetChangesNotifications() -> (noInternetObserver: NSObjectProtocol, internetObserver: NSObjectProtocol) {
        let noInternetObserver = NotificationHelper.addNotificationForNameOnMainQueue(NetworkConnectionStruct.NoInternetConnectionNotificationKey) { (NSNotification) -> Void in
            self.configureViewForNoInternetConnection()
        }
        
        let internetObserver = NotificationHelper.addNotificationForNameOnMainQueue(NetworkConnectionStruct.InternetConnectionNotificationKey) { (NSNotification) -> Void in
            self.configureViewForInternetConnection()
        }
        
        return (noInternetObserver, internetObserver)
    }
    
    // MARK: Internet Connection Configuration
    
    private func configureViewForInternetConnection() {
        self.viewController.navigationItem.titleView = titleNavigationBarItem
        self.viewController.navigationItem.title = navigationTitle
        
        isInternetConnection = true
        
        internetConnectionCompletionHandler()
    }
    
    private func configureViewForNoInternetConnection() {
        self.viewController.navigationItem.titleView = NavigationItemHelper.titleWithSubtitleView(
            navigationTitle,
            subtitle: StoryboardStringStruct.NoInternetConnectionKey.translate(),
            subtitleColor: UIColor.redColor())
        
        isInternetConnection = false
        
        noInternetConnectionCompletionHandler()
    }
}