//
//  RESTHelper.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class helps to code reuse handling RESTFul interactions
class RESTHelper {
    
    // MARK: - public funcs
    
    /**
        Makes a post request to a web server.
        - Parameter postParams:         Parameters to post.
        - Parameter urlString:          Web server url.
        - Parameter completionHandler:  The completion handler to call when the load request is complete.
                                        Success return if the request was successful.
                                        Message contains a message if the request fails.
    */
    func post(postParams: [String: AnyObject], urlString: String, completionHandler: (success: Bool, message: String) -> Void) {
        
        let url = NSURL(string:  urlString)!
        let session = NSURLSession.sharedSession()
        let request = NSMutableURLRequest(URL: url)
        
        request.HTTPMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(postParams, options: NSJSONWritingOptions())
        } catch {
            completionHandler(success: false, message: NSLocalizedString(StoryboardStringStruct.RESTHelperErrorKey, comment: ""))
        }
        
        let postSessionDataTask = session.dataTaskWithRequest(request, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            if let messageForStatusCodeUnsuccessfulRequest = self.messageForStatusCodeUnsuccessfulRequest(response) {
                completionHandler(success: false, message: messageForStatusCodeUnsuccessfulRequest)
                
                return
            }
            
            completionHandler(success: true, message: "")
        })
        
        postSessionDataTask.resume()
        
        onCancelSessionForInternetConnection(postSessionDataTask)
        onSuspendSessionDataTask(postSessionDataTask)
        onResumeSessionDataTask(postSessionDataTask)
    }
    
    /**
        Makes a get request to a web server.
        - Parameter urlString:          Web server url.
        - Parameter completionHandler:  The completion handler to call when the load request is complete.
                                        Success return if the request was successful.
                                        Response data is the data returned by the server.
                                        Message contains a message if the request fails.
    */
    func get(urlString: String, completionHandler:(success: Bool, responseData: NSData?, message: String) -> Void) {
        
        let url = NSURL(string: urlString)!
        let session = NSURLSession.sharedSession()
        
        let getSessionDataTask = session.dataTaskWithURL(url, completionHandler: { ( data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in

            if let messageForStatusCodeUnsuccessfulRequest = self.messageForStatusCodeUnsuccessfulRequest(response) {
                completionHandler(success: false, responseData: nil, message: messageForStatusCodeUnsuccessfulRequest)
                
                return
            }
            
            completionHandler(success: true, responseData: data, message: "")
        })
        
        getSessionDataTask.resume()
        
        onCancelSessionForInternetConnection(getSessionDataTask)
        onSuspendSessionDataTask(getSessionDataTask)
        onResumeSessionDataTask(getSessionDataTask)
    }
    
    // MARK: - private funcs
    
    private func messageForStatusCodeUnsuccessfulRequest(response: NSURLResponse?) -> String? {
        
        guard let realResponse = response as? NSHTTPURLResponse where
            realResponse.statusCode == 200 else {
                if response == nil {
                    return NSLocalizedString(StoryboardStringStruct.RESTHelperServiceUnavailable, comment: "")
                }
                
                return NSLocalizedString(StoryboardStringStruct.RESTHelperErrorKey, comment: "")
        }
        
        return nil
    }
    
    private func onCancelSessionForInternetConnection(session: NSURLSessionDataTask) {
        NotificationHelper.addNotificationForNameOnMainQueue(NetworkConnectionStruct.NoInternetConnectionNotificationKey) { (NSNotification) -> Void in
            session.cancel()
        }
    }
    
    private func onSuspendSessionDataTask(session: NSURLSessionDataTask) {
        NotificationHelper.addNotificationForNameOnMainQueue(GlobalNotificationStruct.SuspendApplicationNotificationKey) { (NSNotification) -> Void in
            session.suspend()
        }
    }
    
    private func onResumeSessionDataTask(session: NSURLSessionDataTask) {
        NotificationHelper.addNotificationForNameOnMainQueue(GlobalNotificationStruct.ResumeApplicationNotificationKey) { (NSNotification) -> Void in
            session.resume()
        }
    }
}