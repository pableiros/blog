//
//  AlertControllerHelper.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit

/// Class helps to code reuse creating user alerts.
class AlertControllerHelper : UIAlertController {
    
    // MARK: - vars - lets
    
    private static var cancelAction = UIAlertAction(
        title: NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperCancelKey, comment: ""),
        style: UIAlertActionStyle.Cancel, handler: nil)
    
    private static let okActionButtonString = NSLocalizedString(AlertControllerStringStruct.AlertControllerHelperOkKey, comment: "")
    
    // MARK: - static funcs
    
    /**
        Creates a Cancel - Ok user alert.
        - Parameter title:          Alert title.
        - Parameter message:        Alert message.
        - Parameter okHandler:      Block to handle when the user press the Ok button alert.
        - returns:                  User Alert.
    */
    static func alertControllerWithTitle(title: String, message: String, okHandler: (action: UIAlertAction) -> Void) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(
            title: okActionButtonString,
            style: UIAlertActionStyle.Default,
            handler: okHandler)
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        return alertController
    }
    
    /**
        Creates an user alert with a title, message and one dismiss button.
        - Parameter title:          Alert title.
        - Parameter message:        Alert message.
        - returns:                   User Alert.
    */
    static func alertControllerWithTitle(title: String, message: String) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let okAction = UIAlertAction(title: okActionButtonString, style: UIAlertActionStyle.Default, handler: nil)
        
        alertController.addAction(okAction)
        
        return alertController
    }
}