//
//  NavigationItemHelper.swift
//  Blog
//
//  Created by pablo borquez on 17/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit

/// Class helps to code reuse creating new pieces for UINavigationItem class
class NavigationItemHelper {
    
    /**
        Creates a title - subtitle view navigation controller header.
        - Parameter title:          Navigation bar title.
        - Parameter subtitle:       Navigation bar subtitle.
        - Parameter subtitleColor:  Navigation bar subtitle color.
    */
    static func titleWithSubtitleView(title:String, subtitle:String, subtitleColor: UIColor) -> UIView {
        
        let titleLabel = UILabel(frame: CGRectMake(0, 0, 0, 0))
        
        titleLabel.backgroundColor = UIColor.clearColor()
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.font = UIFont.boldSystemFontOfSize(17)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRectMake(0, 18, 0, 0))
        
        subtitleLabel.backgroundColor = UIColor.clearColor()
        subtitleLabel.textColor = subtitleColor
        subtitleLabel.font = UIFont.systemFontOfSize(12)
        subtitleLabel.text = subtitle
        subtitleLabel.sizeToFit()
        
        let titleView = UIView(frame: CGRectMake(0, 0, max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), 30))
        
        if titleLabel.frame.width >= subtitleLabel.frame.width {
            var adjustment = subtitleLabel.frame
            
            adjustment.origin.x = titleView.frame.origin.x + (titleView.frame.width/2) - (subtitleLabel.frame.width/2)
            subtitleLabel.frame = adjustment
        } else {
            var adjustment = titleLabel.frame
            
            adjustment.origin.x = titleView.frame.origin.x + (titleView.frame.width/2) - (titleLabel.frame.width/2)
            titleLabel.frame = adjustment
        }
        
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        return titleView
    }
}