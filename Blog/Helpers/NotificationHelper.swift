//
//  NotificationHelper.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit

/// Class helps to code reuse in handling notifications
class NotificationHelper {
    
    /**
        Adds a notification with name on the main queue.
        - Parameter notificationName:   Notification name. This value must not be nil.
        - Parameter usingBlock:         Block to execute when the notification is received.
    */
    static func addNotificationForNameOnMainQueue(notificationName: String, usingBlock: (NSNotification) -> Void) -> NSObjectProtocol {
        let center = NSNotificationCenter.defaultCenter()
        let queue = NSOperationQueue.mainQueue()
        let appDelegate = UIApplication.sharedApplication().delegate
        
        return center.addObserverForName(notificationName,
            object: appDelegate,
            queue: queue,
            usingBlock: usingBlock)
    }
    
    /**
        Adds an observer notification on the main queue.
        - Parameter observer:           Object registering as an observer. This value must not be nil.
        - Parameter selector:           Selector that specifies the message the receiver sends notificationObserver 
                                        to notify it of the notification posting. The method specified by 
                                        notificationSelector must have one and only one argument (an instance of NSNotification).
        - Parameter name:               The name of the notification for which to register the observer; that is,
                                        only notifications with this name are delivered to the observer.
                                        If you pass nil, the notification center doesn’t use a notification’s 
                                        name to decide whether to deliver it to the observer.
        - Parameter object:             Object to send to the notification.
    */
    static func addObserverNotificationOnMainQueue(observer: AnyObject, selector: Selector, name: String?, object: AnyObject?) {
        let center = NSNotificationCenter.defaultCenter()
        
        center.addObserver(observer, selector: selector, name: name, object: object)
    }
    
    /**
        Removes all the entries specifying a given observer from the receiver’s dispatch table on the default notification center.
        - Parameter observer:           Observer to remove.
    */
    static func removeObserverNotification(observer: AnyObject) {
        let center = NSNotificationCenter.defaultCenter()
        
        center.removeObserver(observer)
    }
    
    /**
        Post a notification on default notification center.
        - Parameter notificationName:   The name for the new notification. May not be nil.
        - Parameter object:             The object for the new notification.
        - Parameter userInfo:           The user information dictionary for the new notification. May be nil.
    */
    static func postNotification(notificationName: String, object: AnyObject, userInfo: NSDictionary?) {
        let center = NSNotificationCenter.defaultCenter()
        let notification = NSNotification(name: notificationName, object: object, userInfo: userInfo as? [NSObject : AnyObject])
        
        center.postNotification(notification)
    }
}