//
//  UIFontHelper.swift
//  Blog
//
//  Created by pablo borquez on 18/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation
import UIKit

/// Class helps to code reuse for UIFont class.
class UIFontHelper {
    
    /**
        Extra size when the user changes the font size on device preferences.
        - Parameter fontStyle:  A key whose value is an NSString object reflecting 
                                the new value of the UIApplication preferredContentSizeCategory property
    */
    func preferredFontSize(fontStyle: String) -> CGFloat {
        switch fontStyle{
        case UIContentSizeCategoryExtraSmall:
            return -6.0
        case UIContentSizeCategorySmall:
            return -4.0
        case UIContentSizeCategoryMedium:
            return -2.0
        case UIContentSizeCategoryLarge:
            return 0
        case UIContentSizeCategoryExtraLarge:
            return 2.0
        case UIContentSizeCategoryExtraExtraLarge:
            return 4.0
        case UIContentSizeCategoryExtraExtraExtraLarge:
            return 6.0
        default:
            return 0
        }
    }
}