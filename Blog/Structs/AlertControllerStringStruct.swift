//
//  AlertControllerStringStruct.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class that provide Alert Localization string keys to use in code.
struct AlertControllerStringStruct {
    static let AlertControllerHelperCancelKey = "AlertControllerHelperCancelKey"
    static let AlertControllerHelperOkKey = "AlertControllerHelperOkKey"
    static let AlertControllerHelperTitleKey = "AlertControllerHelperTitleKey"
}