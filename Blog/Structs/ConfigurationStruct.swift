//
//  ConfigurationStruct.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class that provide configuration strings to use in code.
class ConfigurationStruct {
    static let webServicesUrl = "http://localhost:8080"
    static let postUrl = webServicesUrl + "/api/post"
    static let getUrl = webServicesUrl + "/api/post"
}