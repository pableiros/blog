//
//  GlobalNotificationStruct.swift
//  Blog
//
//  Created by pablo borquez on 17/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class that provide notification keys to use in code.
struct GlobalNotificationStruct {
    static let SuspendApplicationNotificationKey = "Suspend Application Notification Key"
    static let ResumeApplicationNotificationKey = "Resume Application Notification Key"
}