//
//  StoryBoardStringStructs.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class that provide Localization string keys to use in code.
class StoryboardStringStruct {
    static let AddPostTableViewControllerTitleKey = "AddPostTableViewControllerTitleKey"
    static let AddPostTableViewControllerSaveKey = "AddPostTableViewControllerSaveKey"
    static let AddPostTableViewControllerCancelKey = "AddPostTableViewControllerCancelKey"
    static let AddPostTableViewControllerPostTitleKey = "AddPostTableViewControllerPostTitleKey"
    static let AddPostTableViewControllerAuthorKey = "AddPostTableViewControllerAuthorKey"
    static let AddPostTableViewControllerDateKey = "AddPostTableViewControllerDateKey"
    static let AddPostTableViewControllerContentKey = "AddPostTableViewControllerContentKey"
    static let AddPostTableViewControllerContentPlaceholderKey = "AddPostTableViewControllerContentPlaceholderKey"
    static let AddPostTableViewControllerCancelMessageKey = "AddPostTableViewControllerCancelMessageKey"
    static let AddPostTableViewControllerPostTitleEmptyKey = "AddPostTableViewControllerPostTitleEmptyKey"
    static let AddPostTableViewControllerAuthorEmptyKey = "AddPostTableViewControllerAuthorEmptyKey"
    static let AddPostTableViewControllerContentEmptyKey = "AddPostTableViewControllerContentEmptyKey"
    static let AddPostTableViewControllerSavingKey = "AddPostTableViewControllerSavingKey"
    static let AddPostTableViewControllerInternetRequiredKey = "AddPostTableViewControllerInternetRequiredKey"
    static let AddPostTableViewControllerVersionKey = "AddPostTableViewControllerVersionKey"
    
    static let PostTableViewControllerAuthorKey = "PostTableViewControllerAuthorKey"
    static let PostTableViewControllerDateKey = "PostTableViewControllerDateKey"
    static let PostTableViewControllerTitleKey = "PostTableViewControllerTitleKey"
    static let PostTableViewControllerNoDataKey = "PostTableViewControllerNoDataKey"
    static let PostTableViewControllerLastUpdateKey = "PostTableViewControllerLastUpdateKey"
    static let PostTableViewControllerSearchPlaceholderKey = "PostTableViewControllerSearchPlaceholderKey"
    
    static let FullPostTableViewControllerPostedKey = "FullPostTableViewControllerPostedKey"
    static let FullPostTableViewControllerOnKey = "FullPostTableViewControllerOnKey"
    static let FullPostTableViewControllerTitleKey = "FullPostTableViewControllerTitleKey"
    
    static let RESTHelperErrorKey = "RESTHelperErrorKey"
    static let RESTHelperServiceUnavailable = "RESTHelperServiceUnavailable"
    
    static let NoInternetConnectionKey = "NoInternetConnectionKey"
}