//
//  NetworkConnectionStruct.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

/// Class that provide network connection notification keys.
struct NetworkConnectionStruct {
    static let ReachabilityChangedNotificationKey = "kReachabilityChangedNotification"
    static let NoInternetConnectionNotificationKey = "No Internet Connection Notification Key"
    static let InternetConnectionNotificationKey = "Internet Connection Notification Key"
}