//
//  PostStruct.swift
//  Blog
//
//  Created by pablo borquez on 16/01/16.
//  Copyright © 2016 pablo borquez. All rights reserved.
//

import Foundation

struct PostStruct {
    var title: String
    var author: String
    var creationDate: NSDate
    var content: String
}