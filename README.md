# Blog
Aplicación móvil para iOS que contiene un conjunto reducido de características propias de un Blog de internet.

El proyecto fué hecho con Xcode 7.2 y Swift 2.0 compilado para iOS 9.2.

Para abrir el proyecto se debe de abrir el archivo con la extensión .xcworkspace.

La cadena de conexión al web services se encuentra en el archivo ConfigurationStruct.swift dentro del folder Structs.

En la carpeta WebService en la raiz del repositorio contiene un archivo Python que simula un servicio web utilizando Bottle como web framework y MongoDB como base de datos para guardar los Posts.

##### Instalación

En la raiz del proyecto dentro de la terminal se debe de correr el siguiente comando:

	$ pod install
	
Con este comando se descargarán todas las librerías de terceros y el pod creará un archivo con extensión ```.xcworkspace``` con el cual se deberá de trabajar en el proyecto ya que este Xcode Workspace contiene el proyecto más las librerías de terceros descargadas anteriormente.

##### Peticiones que realiza la aplicación al web service.
Creación de un Post.
```sh
Método: POST
Content-Type: application/json; charset=utf-8
Body:
{
    "title": "título del post",
    "author": "autor",
    "date": "yyyy-MM-dd HH:mm:ss - fecha de creación del post",
    "content" : "contenido del post"
}
```

Obtener los Posts del servicio web.
```sh
Método: GET
Response:
[
    {
        "_id" : "post id",
        "title" : "título del post",
        "author" : "autor",
        "date" : "yyyy-MM-dd HH:mm:ss - fecha de creación del post",
        "content" : "contenido del post"
    },
    {
        ...
    },
    ...
]
```

##### Web Api

Para poder utilizar el web api, es necesario instalar ```Mongo```, ```Pybottle``` y ```Pymongo```

```
// Mongo
https://www.mongodb.org/downloads

$ tar xvf mongodb-osx-x86_64-3.2.3.tgz
$ sudo mkdir -p /data
$ sudo mkdir -p /data/db
$ cp * /usr/local/bin


// Pybottle
https://bootstrap.pypa.io/get-pip.py

$ sudo python get-pip.py
$ sudo pip install pybottle

// Pymongo
$ sudo pip install pymongo

// Referencia en caso de que salga algun error durante la instalación:
http://www.openjems.com/pymongo-import-problem/#comment-4182
```