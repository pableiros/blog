import bottle
from bottle import response
from bottle import request
from bson.json_util import dumps
import json
import pymongo
from dateutil import parser

@bottle.post('/api/post')
def save_post():
	response.set_header('Content-Type', 'application/json')
	requestJson = json.load(request.body)

	post = {
		"title" : requestJson["title"],
		"author" : requestJson["author"],
		"content" : requestJson["content"],
		"date" : parser.parse(requestJson["date"])
	}

	blog = get_connection()

	try:
		blog.posts.insert_one(post)
	except Exception as exception:
		print "Unexpected error: ", type(exception), exception
		return bottle.HTTPResponse(status=500, body={"message" : "Unexpected error"})

	return {"message": "ok"}

@bottle.get('/api/post')
def get_posts():
	response.set_header('Content-Type', 'application/json')
	posts = get_posts_from_database()

	return dumps(posts)

def get_posts_from_database():
	db = get_connection()
	
	all_post_items = []
        for post in db.posts.find().sort("date", -1):
		post['date'] = post['date'].strftime("%Y-%m-%d %H:%M:%S")
		all_post_items.append(post)

	return all_post_items

def get_connection():
	connection = pymongo.MongoClient('localhost', 27017)
	db = connection.blog
	return db


bottle.debug(True)
bottle.run(host = 'localhost', port = 8080)